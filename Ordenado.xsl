<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/menu">
        <menu>
            <xsl:for-each select="llista">
                <llista>
                    <xsl:copy-of select="nom"/>
                    <plats>
                    <xsl:for-each select="plats/plat">
                        <xsl:sort select="nom" data-type="text" order="ascending"/>
                        <plat>
                            <xsl:copy-of select="nom"/>
                            <xsl:for-each select="ingredients/ingredient">
                            <xsl:sort select="quantitat" data-type="number" order="ascending"/>
                            <ingredient>
                                <xsl:copy-of select="nom"/>
                                <quantitat>
                                    <xsl:attribute name="Unit">
                                        <xsl:value-of select="quantitat/@unitat"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="quantitat"/>
                                </quantitat>
                            </ingredient>
                        </xsl:for-each>
                        </plat>
                    </xsl:for-each>
                    </plats>
                </llista>
            </xsl:for-each>
        </menu> 
    </xsl:template>
</xsl:stylesheet>