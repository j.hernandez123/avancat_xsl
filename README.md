![](IMG/Sin título-2.png)

#  :two_men_holding_hands: Autors:
    Christian Gascón García
    Javier Hernández Ortega

##  :calendar: Dates:

 `Data de sortida:      13/04/2018 - 00:00`  
 `Última Actualitzaciò: 13/04/2018 - 15:33`
 ----

###  :diamond_shape_with_a_dot_inside: Exercici 1</br>
`Tematica Utilitzada: ` **Fosses comunes.**  <br/>  
`Documents XML utilitzats:`  <br/>  
* :closed_book: : [XSLT Avançat.xml](https://gitlab.com/j.hernandez123/avancat_xsl/blob/master/XSLT%20AVANCAT.xml)
* :closed_book: : [Fosa.xsl](https://gitlab.com/j.hernandez123/avancat_xsl/blob/master/Fosa.xsl)

>  :star2: Abans de començar a tractar l'XML, en Christian i jo varem dilucidar sobre quina seria la informació més adient a l'hora de mostrar el XLS final, perque dins del XML hi havia molta informació que per nosaltres era totalment irrelevant. Finalment el vàrem mostrar d'aquesta manera: 

  <br/>  
#### Informació a mostrar  <br/>  
----
**Filtrat per:** _Comarques (La selva i Maresme)_  <br/>  
**Dades a filtrar:**  
* _Títol_
* _Bàndol_
* _Tipus de fossa_
* _Comarca situada_
* _Atribut Link amb el link de fosses de gencat_

  <br/>  

#### :camera: _Imatge del XML i XLS_

![](IMG/Ex1.png)

#### :camera: _Imatge del resultat d'execució_  <br/>  

![](IMG/Ex1_Final.png)  <br/>
----

##  :diamond_shape_with_a_dot_inside: Exercici 2 </br>  

#### Versio 1:
`Documents XML utilitzats:` </br>  
* :closed_book: : [xml.xml](https://gitlab.com/j.hernandez123/avancat_xsl/blob/master/xml.xml) </br>  
* :closed_book: : [Traducido.xsl](https://gitlab.com/j.hernandez123/avancat_xsl/blob/master/Traducido.xsl) </br>  


> Com podem observar, al arxiu **"xml.xml"** podem veure tots els atributs i les etiquetes en l'idioma Català, amb l'us de l'arxiu **"Traducido.xsl"** a l'executarlo canvien totes les etiquetes del xml a l'idioma Angles.

#### :camera: _Imatge del XML i XLS_  <br/>

![](IMG/Ex2.png)  <br/>

#### :camera: _Imatge del resultat d'execució_  <br/>  

![](IMG/Ex2_Final.png)  <br/>
----

#### Versio 2:
`Documents XML utilitzats:` <br/>  

* :closed_book: : [xml.xml](https://gitlab.com/j.hernandez123/avancat_xsl/blob/master/xml.xml) </br>  
* :closed_book: : [Ordenado.xsl](https://gitlab.com/j.hernandez123/avancat_xsl/blob/master/Ordenado.xsl) </br>  

> Com podem observar, a l'arxiu **"xml.xml"** podem veure totes les dades desordenades de una manera quasi caotica, amb l'us de l'arxiu **"Ordenado.xsl"** obtendrem i l'ordenarem de una manera que anomenarem acontinuació:

#### **Filtrat:**
* _Tipus d'àpat(Esmozar,Dinar,Brenar,Sopar)._
* _Nom del plat._
* _Nom d'ingredients._
* _Quantitat d'ingredients._ 


#### **Ordenat:**
* _Nom del plat (Acendent)._
* _Quantitat d'ingredients (Ascendent)._ 


#### :camera: _Imatge del XML i XLS_  <br/>  
![](IMG/Ex3.png)  <br/>

#### :camera: _Imatge del resultat d'execució_  <br/>  
![](IMG/Ex3_Final.png)  <br/>


----