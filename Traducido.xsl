<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/menu">
        <menu>
            <xsl:for-each select="llista">
                <list>
                    <name>
                        <xsl:value-of select="nom"/>
                    </name>
                    <dishes>
                    <xsl:for-each select="plats/plat">
                        <dish>
                            <name>
                                <xsl:value-of select="nom"/>
                            </name>
                        <ingredients>
                        <xsl:for-each select="ingredients/ingredient">
                            <ingredient>
                                <name>
                                    <xsl:value-of select="nom"/>
                                </name>
                                <quantity>
                                    <xsl:attribute name="Unit">
                                        <xsl:value-of select="quantitat/@unitat"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="quantitat"/>
                                </quantity>
                            </ingredient>
                        </xsl:for-each>
                        </ingredients>
                        </dish>
                    </xsl:for-each>
                    </dishes>
                </list>
            </xsl:for-each>
        </menu> 
    </xsl:template>
</xsl:stylesheet>