<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/response/row">
        <commonpit>
            <xsl:for-each select="row[comarca = 'Selva']">
                <fosa>
                    <xsl:copy-of select="titol"/>
                    <xsl:copy-of select="bandol"/>
                    <tipus>
                        <xsl:value-of select="tipusfossa"/>
                    </tipus>
                    <xsl:copy-of select="comarca"/>
                    <link>
                        <xsl:attribute name="link">
                            <xsl:value-of select="fitxa/@url"/>
                        </xsl:attribute>
                    </link>
                </fosa>
            </xsl:for-each>
        </commonpit> 
    </xsl:template>
</xsl:stylesheet>